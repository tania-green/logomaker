# Logo Manipulator

> Basic logo manipulator application with the following features:
● “Undo” - After making a change on the canvas, this will allow you to undo one step at a time
● “Redo” - After performing an undo, this will re-apply the change. Performing an ‘undo’ 5
times will allow you to perform 5 ‘redo’ actions to get back where you were.
● “Reset” - Sets the canvas back to the original state. This action can also be ‘undone’

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
