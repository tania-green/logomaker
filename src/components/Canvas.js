import { fabric } from 'fabric'
import logo from '../images/designcrowd-logo.png'

export default {
  data() {
    return {
      canvas: null,
      history: [],
      currentIndex: 0,
      isResetted: false,
    }
  },
  methods:{
    init(){
      this.canvas = new fabric.Canvas('c1',
      {
        backgroundColor: 'grey',
        selectionColor: 'grey',
        selectionBorderColor: 'grey',
        selectionLineWidth: 2
      });
  
      this.canvas.observe('object:modified', () => {
        this.saveHistory();
      });
    },
    saveHistory() {     
      this.history.push(JSON.stringify(this.canvas.toJSON()));
      this.currentIndex = this.history.length - 1;
    },
    undo() {
      if(this.isResetted) {
        this.isResetted = false;
        this.currentIndex = this.history.length -1;
      } else {
        this.currentIndex -= 1;
      }    
      const parsedData = JSON.parse(this.history[this.currentIndex]);
      this.canvas.loadFromJSON(parsedData);
    },
    redo() {
      this.currentIndex += 1;
      const parsedData = JSON.parse(this.history[this.currentIndex]);
      this.canvas.loadFromJSON(parsedData);
    },
    reset() {
      this.currentIndex = 0;
      this.isResetted = true;
      const parsedData = JSON.parse(this.history[this.currentIndex]);
      this.canvas.loadFromJSON(parsedData);
    },
  },
  mounted(){
    this.init();

    const logoText = new fabric.Text("My Logo", {
      fontFamily: 'Comic Sans',
      fontWeight: 'bold',
      fontSize: 45,
      textAlign: 'center',
      top: 30,
    });

    logoText.setColor('blue');

    const imgElement = new Image();
    imgElement.src = logo;
    imgElement.addEventListener("load", () => {
      const imgInstance = new fabric.Image(imgElement);
  
    this.canvas.add(logoText);
    this.canvas.add(imgInstance);

    logoText.centerH();
    imgInstance.center();
    
    this.saveHistory();
    });
  },
}